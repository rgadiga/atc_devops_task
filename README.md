I have deployed nodejs application in two ways 
1) simple way
2) Helm with LB and ssl
 
gitlab cicd pipeline is also mentioned in helm folder for helm deployment and in root folder for simple way of deployment.

Creating Kubernetes cluster steps are mentioned in Create_Kubernetes_cluster file


# Kubernetes Hello World Node.js

This project is intended to be used as a sample for 
deployment of kubernetes applications. 

Basically this expose 3 apis:
- /ping
- /current-date
- /helloworld

The last one is good to test the capacity of the cluster by implement 
a fibonnaci with any number specified. Numbers great than 35 could crash your server :)

# Start

Build the image

```
docker build -t rgadiga/k8s-hello-node
```

Run the Image

```
docker run -d -p 3000:3000 rgadiga/k8s-hello-node
```

# Deploying in the Kubernetes cluster

First create the Pods and the autoscale 

```
kubectl apply -f kubernetes/deployment.yml
```

Check if is OK:

```
kubectl get pods -l app=k8s-node -o yaml | grep podIP
```

# Exposing the Api to the World:

```
kubectl apply -f kubernetes/service-external.yml
```

Checking:

```
kubectl get service
```

Run this to see the auto scaling working: https://www.skenz.it/cs/ab
How do you make the service scalable?

```
ab -n 500 -c 10  http://<IP>/helloworld
```

# Exposing the Api inside the cluster:


```
kubectl apply -f kubernetes/service-external.yml
```

Checking:

```
kubectl run terminal --generator=run-pod/v1 --image=alpine:3.8 -i --tty
```

How would you store and deploy secrets (such as API keys) 
https://kubernetes.io/docs/concepts/configuration/secret/
https://kubernetes.io/docs/tasks/configmap-secret/managing-secret-using-kubectl/



SSL certificate:
https://kubernetes-on-aws.readthedocs.io/en/latest/user-guide/ingress.html
