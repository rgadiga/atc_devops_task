let express = require('express');
let app = express();

app.get('/', function (req, res) {
  let obj = {
    endpoints: [
      "/ping",
      "/current-date",
      "/helloworld",
    ]
  };
  res.send(obj);
});

app.get('/ping', function (req, res) {
  res.send("pong");
});


app.get('/current-date', function (req, res) {
  let obj = {
    name: "current",
    value: new Date()
  };
  res.send(obj);
});

app.get('/helloworld', function (req, res) {
  res.send("Hello World !");
});


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

